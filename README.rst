Représentation graphique des tables de multiplication en Python
===============================================================

Ce programme a été créé après la visualisation de la vidéo "La face cachée des tables de multiplication" (Micmaths) :

    https://www.youtube.com/watch?v=-X49VQgi86E

Dépendances
-----------

Le programme utilise la librairie pygame_.

.. _pygame: http://www.pygame.org/

Utilisation
-----------

Pour lancer le programme, exécuter :

::

    python tables_de_multiplication.py

Licence
-------

GPL V3
